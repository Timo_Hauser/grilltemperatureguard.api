import sys
sys.dont_write_bytecode = True

# import sessionHelper
# from entities.sensor import Sensor
# from entities.sensorValue import SensorValue
# from entities.servo import Servo
# from entities.servoValue import ServoValue
# from entities.setting import Setting

import libbcm2835._bcm2835 as soc
import time
import os

CMD_RREG = 0x10
CMD_WREG = 0x50
CMD_SYNC = 0xFC
CMD_WAKEUP = 0x00
CMD_RDATA = 0x01

RPI_GPIO_P1_15=22
RPI_GPIO_P1_11=17

BCM2835_GPIO_FSEL_OUTP = 0x01
BCM2835_GPIO_FSEL_INPT =0x00
BCM2835_GPIO_PUD_UP = 0x02

REG_MUX = 1
REG_STATUS = 0

ADS1256_GAIN_1 = (0)
# ADS1256_15SPS = 0b00110011
ADS1256_15SPS = 0x33
# ADS1256_5SPS = 0b00110011

HIGH=0x1
LOW=0x0

class Ads1256():
    def __init__(self):
        self._gain = 0
        self._dataRate = 0
        self._setup()

    def end(self):
        soc.bcm2835_spi_end();
        soc.bcm2835_close();

    def _setup(self):
        soc.bcm2835_init()
        soc.bcm2835_spi_begin()
        soc.bcm2835_spi_setBitOrder(soc.BCM2835_SPI_BIT_ORDER_LSBFIRST )
        soc.bcm2835_spi_setDataMode(soc.BCM2835_SPI_MODE1)
        soc.bcm2835_spi_setClockDivider(soc.BCM2835_SPI_CLOCK_DIVIDER_1024)
        soc.bcm2835_gpio_fsel(RPI_GPIO_P1_15, BCM2835_GPIO_FSEL_OUTP)
        soc.bcm2835_gpio_write(RPI_GPIO_P1_15, HIGH)
        soc.bcm2835_gpio_fsel(RPI_GPIO_P1_11, BCM2835_GPIO_FSEL_INPT)
        soc.bcm2835_gpio_set_pud(RPI_GPIO_P1_11, BCM2835_GPIO_PUD_UP)
        print 'ASD1256 Chip ID: ' + str(self.read_chip_id())
        self._cfg_adc(ADS1256_GAIN_1, ADS1256_15SPS)

    def _wait_drdy(self):
        while (soc.bcm2835_gpio_lev(RPI_GPIO_P1_11) != 0):
            pass

    def _cs_0(self):
        soc.bcm2835_gpio_write(RPI_GPIO_P1_15, LOW)

    def _cs_1(self):
        soc.bcm2835_gpio_write(RPI_GPIO_P1_15, HIGH)

    def _bsp_DelayUS(self, micros):
        soc.bcm2835_delayMicroseconds(micros)

    def _send_8_bit(self, data):
        self._bsp_DelayUS(2)
        soc.bcm2835_spi_transfer(data)

    def _recieve_8_bit(self):
        return soc.bcm2835_spi_transfer(0xff)

    def _delay_data(self):
        self._bsp_DelayUS(15)

    def _read_reg(self, reg):
        self._cs_0()
        self._send_8_bit(CMD_RREG | reg)
        self._send_8_bit(0x00)
        self._delay_data()
        read = self._recieve_8_bit()
        self._cs_1()
        return read

    def _write_reg(self, reg, val):
        self._cs_0()
        self._send_8_bit(CMD_WREG | reg)
        self._send_8_bit(0x00)
        self._send_8_bit(val)
        self._cs_1()

    def _set_channel(self, channel):
        if(channel > 7):
            return
        self._write_reg(REG_MUX, (channel << 4) | (1 << 3))

    def _cfg_adc(self, gain, rate):
        self._gain = gain
        self._dataRate = rate

        self._wait_drdy()
        buf = [0] * 4
        buf[0] = (0 << 3) | (1 << 2) | (0 << 1)
        buf[1] = 0x08
        buf[2] = (0 << 5) | (0 << 3) | (gain << 0)
        buf[3] = rate

        self._cs_0()
        self._send_8_bit(CMD_WREG | 0)
        self._send_8_bit(0x03)

        self._send_8_bit(buf[0])
        self._send_8_bit(buf[1])
        self._send_8_bit(buf[2])
        self._send_8_bit(buf[3])
        self._cs_1()
        self._bsp_DelayUS(5)

    def _read_data(self):
        read = 0
        buf = [0] * 3
        self._cs_0()
        self._send_8_bit(CMD_RDATA)
        self._delay_data()

        buf[0] = self._recieve_8_bit()
        buf[1] = self._recieve_8_bit()
        buf[2] = self._recieve_8_bit()

        read = (buf[0] << 16) & 0x00FF0000
        read |= (buf[1] << 8)
        read |= buf[2]

        self._cs_1()

        if(read & 0x800000):
            read |= 0xFF000000
        return read

    def _write_cmd(self, cmd):
        self._cs_0()
        self._send_8_bit(cmd)
        self._cs_1()

    def read_chip_id(self):
        self._wait_drdy()
        id = self._read_reg(REG_STATUS)
        return id >> 4
        
    def read(self, pin):
        if (pin > 7):
            return 0

        self._wait_drdy()
        self._set_channel(pin)
        self._bsp_DelayUS(5)
        self._write_cmd(CMD_SYNC)
        self._bsp_DelayUS(5)
        self._write_cmd(CMD_WAKEUP)
        self._bsp_DelayUS(25)
        
        time.sleep(1)
        value = self._read_data()
        value = (value * 100) / 167 #convert to voltage
        return float(value) / 1000000

if __name__ == '__main__':
    c = Ads1256()
    
    while(1<2):
        print c.read(0)
        print c.read(1)
        # print c.read(2)
        # print c.read(3)
        # print c.read(4)
        # print c.read(5)
        # print c.read(6)
        # print c.read(7)
        time.sleep(10)

    c.end()