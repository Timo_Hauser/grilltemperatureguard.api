from flask_restful import Resource, fields, marshal_with
from flask import request
from entities.servo import Servo as Entity
import sessionHelper

resource_fields = {
    'id':   fields.Integer,
    'name':    fields.String,
    'active': fields.Boolean,
    'pin': fields.Integer,
    'upperLimit': fields.Integer,
    'lowerLimit': fields.Integer,
    'targetValue': fields.Integer,
    'minimumUs': fields.Integer,
    'maximumUs': fields.Integer
}

class ServoList(Resource):
    @marshal_with(resource_fields)
    def get(self):
        return sessionHelper.open_session().query(Entity).all()

    @marshal_with(resource_fields)
    def post(self):
        servo = Entity()
        servo.name= request.json['name']
        servo.pin =  request.json['pin']
        servo.active = request.json.get('active', False)
        servo.upperLimit= request.json.get('upperLimit', 100)
        servo.lowerLimit= request.json.get('lowerLimit', 0)
        servo.targetValue= request.json.get('targetValue', 0)
        servo.minimumUs = request.json.get('minimumUs', 600)
        servo.maximumUs = request.json.get('maximumUs', 2500)
        session = sessionHelper.open_session()
        session.add(servo)
        session.commit()
        return servo

class Servo(Resource):
    @marshal_with(resource_fields)
    def get(self, servo_id):
        return sessionHelper.open_session().query(Entity).filter(Entity.id==servo_id).first()

    @marshal_with(resource_fields)
    def put(self, servo_id):
        session = sessionHelper.open_session()
        servo = session.query(Entity).filter(Entity.id==servo_id).first()
        servo.name= request.json['name']
        servo.pin =  request.json['pin']
        servo.active = request.json.get('active', False)
        servo.upperLimit= request.json.get('upperLimit', 100)
        servo.lowerLimit= request.json.get('lowerLimit', 0)
        servo.targetValue= request.json.get('targetValue', 0)
        servo.minimumUs = request.json.get('minimumUs', 600)
        servo.maximumUs = request.json.get('maximumUs', 2500)
        session.commit()
        return servo

    @marshal_with(resource_fields)
    def delete(self, servo_id):
        session = sessionHelper.open_session()
        servo = session.query(Entity).filter(Entity.id==servo_id).first()
        session.delete(servo)
        session.commit()