from flask_restful import Resource, fields, marshal_with
from sqlalchemy import func
from entities.servoValue import ServoValue as Entity
import sessionHelper

resource_fields = {
    'id':   fields.Integer,
    'servoId':    fields.Integer,
    'dateTime' : fields.DateTime,
    'value': fields.Float
}

class ServoValueList(Resource):
    @marshal_with(resource_fields)
    def get(self):
        return sessionHelper.open_session().query(Entity).all()

class ServoValue(Resource):
    @marshal_with(resource_fields)
    def get(self, servo_id):
        return sessionHelper.open_session().query(Entity).filter(Entity.servoId==servo_id).all()

class CurrentServoValueList(Resource):
    @marshal_with(resource_fields)
    def get(self):
        session = sessionHelper.open_session()
        t = session.query(Entity.id, func.max(Entity.dateTime).label('max_dateTime')).group_by(Entity.servoId).subquery('t')
        return session.query(Entity).filter(Entity.id == t.c.id).all()

class CurrentServoValue(Resource):
    @marshal_with(resource_fields)
    def get(self, servo_id):
        return sessionHelper.open_session().query(Entity).filter(Entity.servoId == servo_id).order_by(Entity.dateTime.desc()).first()