from flask_restful import Resource, fields, marshal_with
from sqlalchemy import func
from entities.sensorValue import SensorValue as Entity
import sessionHelper

resource_fields = {
    'id':   fields.Integer,
    'sensorId':    fields.Integer,
    'dateTime' : fields.DateTime,
    'value': fields.Float
}

class SensorValueList(Resource):
    @marshal_with(resource_fields)
    def get(self):
        return sessionHelper.open_session().query(Entity).all()

class SensorValue(Resource):
    @marshal_with(resource_fields)
    def get(self, sensor_id):
        return sessionHelper.open_session().query(Entity).filter(Entity.sensorId==sensor_id).all()

class CurrentSensorValueList(Resource):
    @marshal_with(resource_fields)
    def get(self):
        session = sessionHelper.open_session()
        t = session.query(Entity.id, func.max(Entity.dateTime).label('max_dateTime')).group_by(Entity.sensorId).subquery('t')
        return session.query(Entity).filter(Entity.id == t.c.id).all()

class CurrentSensorValue(Resource):
    @marshal_with(resource_fields)
    def get(self, sensor_id):
        return sessionHelper.open_session().query(Entity).filter(Entity.sensorId == sensor_id).order_by(Entity.dateTime.desc()).first()