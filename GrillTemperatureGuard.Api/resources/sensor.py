from flask_restful import Resource, fields, marshal_with
from flask import request
from entities.sensor import Sensor as Entity
import sessionHelper

resource_fields = {
    'id':   fields.Integer,
    'name':    fields.String,
    'activeForRegulation' : fields.Boolean,
    'active' : fields.Boolean,
    'pin': fields.Integer,
    'pullDownResistance' : fields.Integer,
    'b25100Constant' : fields.Float,
    'r25Resistance' : fields.Integer
}

class SensorList(Resource):
    @marshal_with(resource_fields)
    def get(self):
        return sessionHelper.open_session().query(Entity).all()

    @marshal_with(resource_fields)
    def post(self):
        sensor = Entity()
        sensor.name= request.json['name']
        sensor.activeForRegulation= request.json.get('activeForRegulation', False)
        sensor.pin =  request.json['pin']
        sensor.active = request.json.get('active', False)
        sensor.pullDownResistance = request.json.get('pullDownResistance', 100000)
        sensor.b25100Constant = request.json.get('b25100Constant', 0)
        sensor.r25Resistance = request.json.get('r25Resistance', 1000000)
        session = sessionHelper.open_session()
        session.add(sensor)
        session.commit()
        return sensor

class Sensor(Resource):
    @marshal_with(resource_fields)
    def get(self, sensor_id):
        return sessionHelper.open_session().query(Entity).filter(Entity.id==sensor_id).first()

    @marshal_with(resource_fields)
    def put(self, sensor_id):
        session = sessionHelper.open_session()
        sensor = session.query(Entity).filter(Entity.id==sensor_id).first()
        sensor.name= request.json['name']
        sensor.activeForRegulation= request.json.get('activeForRegulation', False)
        sensor.pin =  request.json['pin']
        sensor.active = request.json.get('active', False)
        sensor.pullDownResistance = request.json.get('pullDownResistance', 100000)
        sensor.b25100Constant = request.json.get('b25100Constant', 0)
        sensor.r25Resistance = request.json.get('r25Resistance', 1000000)
        session.commit()
        return sensor


    @marshal_with(resource_fields)
    def delete(self, sensor_id):
        session = sessionHelper.open_session()
        sensor = session.query(Entity).filter(Entity.id==sensor_id).first()
        session.delete(sensor)
        session.commit()