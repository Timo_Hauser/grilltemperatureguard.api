from flask_restful import Resource, fields, marshal_with
from flask import request
from entities.setting import Setting as Entity
import sessionHelper

resource_fields = {
    'id':   fields.Integer,
    'name':    fields.String,
    'value' : fields.String
}

class SettingList(Resource):
    @marshal_with(resource_fields)
    def get(self):
        return sessionHelper.open_session().query(Entity).all()

    @marshal_with(resource_fields)
    def post(self):
        setting = Entity()
        setting.name= request.json['name']
        setting.value =  request.json['value']
        session = sessionHelper.open_session()
        session.add(setting)
        session.commit()
        return setting

class Setting(Resource):
    @marshal_with(resource_fields)
    def get(self, setting_id):
        return sessionHelper.open_session().query(Entity).filter(Entity.id==setting_id).first()

    @marshal_with(resource_fields)
    def put(self, setting_id):
        session = sessionHelper.open_session()
        setting = session.query(Entity).filter(Entity.id==setting_id).first()
        setting.name= request.json['name']
        setting.value =  request.json['value']
        session.commit()
        return setting

    @marshal_with(resource_fields)
    def delete(self, setting_id):
        session = sessionHelper.open_session()
        setting = session.query(Entity).filter(Entity.id==setting_id).first()
        session.delete(setting)
        session.commit()

class SettingValue(Resource):
    def get(self, setting_name):
        setting = sessionHelper.open_session().query(Entity).filter(Entity.name==setting_name).first()
        if (setting is None):
            return None
        return setting.value

    def put(self, setting_name):
        session = sessionHelper.open_session()
        setting = session.query(Entity).filter(Entity.name==setting_name).first()
        if(setting is None):
            setting = Entity()
            setting.name = setting_name
            session.add(setting)
        
        setting.value = request.args.get('value', '')
        session.commit()
        return setting.value