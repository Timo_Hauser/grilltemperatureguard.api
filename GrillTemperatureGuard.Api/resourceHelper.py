
from resources.sensor import Sensor, SensorList
from resources.sensorValue import SensorValue, SensorValueList, CurrentSensorValueList, CurrentSensorValue
from resources.servo import Servo, ServoList
from resources.servoValue import ServoValue, ServoValueList, CurrentServoValueList, CurrentServoValue
from resources.setting import Setting, SettingList, SettingValue

def setup(api):
    api.add_resource(SensorList, '/sensors')
    api.add_resource(Sensor, '/sensors/<int:sensor_id>')

    api.add_resource(SensorValueList, '/sensors/values')
    api.add_resource(SensorValue, '/sensors/<int:sensor_id>/values')
    api.add_resource(CurrentSensorValueList, '/sensors/values/current')
    api.add_resource(CurrentSensorValue, '/sensors/<int:sensor_id>/values/current')

    api.add_resource(ServoList, '/servos')
    api.add_resource(Servo, '/servos/<int:servo_id>')

    api.add_resource(ServoValueList, '/servos/values')
    api.add_resource(ServoValue, '/servos/<int:servo_id>/values')
    api.add_resource(CurrentServoValueList, '/servos/values/current')
    api.add_resource(CurrentServoValue, '/servos/<int:servo_id>/values/current')

    api.add_resource(SettingList, '/settings')
    api.add_resource(Setting, '/settings/<int:setting_id>')
    api.add_resource(SettingValue, '/settings/value/<string:setting_name>')
