import sys
sys.dont_write_bytecode = True

from flask import Flask
from flask_restful import Api
from flask_cors import CORS, cross_origin

import resourceHelper
import sessionHelper

app = Flask(__name__)
CORS(app)
api = Api(app)

if __name__ == '__main__':
    resourceHelper.setup(api)
    app.run(debug=True, host='0.0.0.0')