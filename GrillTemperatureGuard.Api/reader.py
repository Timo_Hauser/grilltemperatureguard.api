import sys
sys.dont_write_bytecode = True

import sessionHelper
from entities.sensor import Sensor
from entities.sensorValue import SensorValue
from entities.servo import Servo
from entities.servoValue import ServoValue
from entities.setting import Setting
from servo import Servo as pwmServo
import ads1256
from datetime import datetime
import time
import os
import math

def __calculateTemperature(r25, b25100Constant, resistance):
    t25 = 298.15
    res = 1 / ((math.log(resistance / r25) / b25100Constant) + (1 / t25))
    return res - 274.15 #Klelvin to Celsius

def __calculateResistance(pullDownResistance, voltage):
    vcc = 5
    res = pullDownResistance * ((vcc - voltage) / voltage)
    return res

def run():
    
    adc = ads1256.Ads1256()

    session = sessionHelper.open_session()
    sensors = session.query(Sensor).all()
    servos = session.query(Servo).all()

    temperatures = list()

    for sensor in sensors:
        if(sensor.name == '' or sensor.active == False):
            continue
        voltage = adc.read(sensor.pin)
        resistance = __calculateResistance(sensor.pullDownResistance, voltage)
        val = __calculateTemperature(sensor.r25Resistance, sensor.b25100Constant, resistance)
        val = round(val)
        print 'Pin ' + str(sensor.pin) + ' : ' + str(voltage) + ' V; ' + str(resistance) + ' Ohm; ' + str(val) + ' deg Celsius'
        value = SensorValue()
        value.sensorId = sensor.id
        value.value = val
        value.dateTime = datetime.now()
        session.add(value)
        if(sensor.activeForRegulation == True):
            temperatures.append(val)            

    avgTemp = sum(temperatures) / len(temperatures)
    stepWidth = float(session.query(Setting).filter(Setting.name=='servoStepWidth').first().value)
    targetTemperature = float(session.query(Setting).filter(Setting.name=='temperature').first().value)
    for servo in servos:
        if(servo.name == '' or servo.active == False):
            continue
        if(avgTemp == targetTemperature):
            continue
        if(avgTemp < targetTemperature and servo.targetValue <= (servo.upperLimit - stepWidth)):
            servo.targetValue += stepWidth
        if(avgTemp > targetTemperature and servo.targetValue >= (servo.lowerLimit + stepWidth)):
            servo.targetValue -= stepWidth

        pwmServo(servo.pin, servo.minimumUs, servo.maximumUs).set(servo.targetValue)
        value = ServoValue()
        value.servoId = servo.id
        value.value = servo.targetValue
        value.dateTime = datetime.now()
        session.add(value)

    session.commit()
    print
    time.sleep(10)

    adc.end()

if __name__ == '__main__':
    while(True):
        run()    