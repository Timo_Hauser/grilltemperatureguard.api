import sys
sys.dont_write_bytecode = True

import time
import os
from RPIO import PWM

gpio_pwm = PWM.Servo()

class Servo():
    def __init__(self, pin, min_us, max_us):
        self.pin = pin
        self.min_us = min_us
        self.max_us = max_us        

    def set(self, value):
        if(value > 0):
            value = (self.max_us - self.min_us) * (float(value) / 100) + self.min_us
            value = int(round(value / 10) * 10)
        else:
            value = self.min_us
        gpio_pwm.set_servo(self.pin, value)

    def stop(self):
        gpio_pwm.stop_servo(self.pin)

if __name__ == '__main__':

    pin = 14  
    servo = Servo(pin, 600, 2500)
    
    val = 0
    servo.set(0)
    time.sleep(1)
    servo.set(100)
    time.sleep(1)
    servo.set(50)
    time.sleep(1)
    servo.set(0)
    time.sleep(1)
    while(val <= 100):
        servo.set(val)
        time.sleep(0.05)
        val += 0.5

    while(val >= 0):
        servo.set(val)
        time.sleep(0.05)
        val -= 0.5

    servo.stop()