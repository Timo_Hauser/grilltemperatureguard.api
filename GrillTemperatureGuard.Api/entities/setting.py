from sqlalchemy import Column, ForeignKey, Integer, String
from entities.base import Base
from sqlalchemy.orm import relationship
from entities.base import Base

class Setting(Base):
    __tablename__ = 'Settings'

    #Columns
    id = Column(Integer, primary_key = True, autoincrement=True)
    name = Column(String(25))
    value = Column(String(25))