from sqlalchemy import Column, ForeignKey, Integer, String, DATETIME, Float
from sqlalchemy.orm import relationship
from entities.base import Base

class ServoValue(Base):
    __tablename__ = 'ServoValues'

    #Columns
    id = Column(Integer, primary_key = True, autoincrement=True)
    servoId = Column(Integer)
    dateTime = Column(DATETIME)
    value = Column(Float)