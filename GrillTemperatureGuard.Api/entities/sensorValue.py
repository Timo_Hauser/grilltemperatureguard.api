from sqlalchemy import Column, ForeignKey, Integer, String, DATETIME, Float
from entities.base import Base
from sqlalchemy.orm import relationship

class SensorValue(Base):
    __tablename__ = 'SensorValues'

    #Columns
    id = Column(Integer, primary_key = True, autoincrement=True)
    sensorId = Column(Integer)
    dateTime = Column(DATETIME)
    value = Column(Float)