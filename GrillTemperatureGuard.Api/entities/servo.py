from sqlalchemy import Column, ForeignKey, Integer, String, Boolean
from sqlalchemy.orm import relationship
from entities.base import Base

class Servo(Base):
    __tablename__ = 'Servos'

    #Columns
    id = Column(Integer, primary_key = True, autoincrement=True)
    name = Column(String(25))
    active = Column(Boolean)
    pin  = Column(Integer)
    upperLimit = Column(Integer)
    lowerLimit = Column(Integer)
    targetValue = Column(Integer)
    minimumUs = Column(Integer)
    maximumUs = Column(Integer)