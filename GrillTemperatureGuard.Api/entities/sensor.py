from sqlalchemy import Column, ForeignKey, Integer, String, Float, Boolean
from entities.base import Base

class Sensor(Base):
        __tablename__ = 'Sensors'

        #Columns
        id = Column(Integer, primary_key = True, autoincrement=True)
        name = Column(String(25), unique = True)
        activeForRegulation = Column(Boolean)
        active = Column(Boolean)
        pin = Column(Integer)
        pullDownResistance = Column(Integer)
        b25100Constant = Column(Float)
        r25Resistance = Column(Integer)