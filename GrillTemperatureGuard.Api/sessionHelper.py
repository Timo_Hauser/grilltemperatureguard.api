from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

"""
Here all used entities need to be imported, so they are known in SqlAlchemy and the database gets created correctly
"""

from entities.base import Base
from entities.sensor import Sensor
from entities.sensorValue import SensorValue
from entities.servo import Servo
from entities.servoValue import ServoValue
from entities.setting import Setting

DATABASE_PATH = 'db/database.sqlite3'
DATABASE_DRIVER = 'sqlite:///'

#SQLAlchemy setup
engine = create_engine(DATABASE_DRIVER + DATABASE_PATH, echo=False)
#Show SQL
#engine = create_engine(DATABASE_DRIVER + DATABASE_PATH, echo=True)

Base.metadata.create_all(engine)

# This session class is used in all database accesses
Session = sessionmaker(bind=engine)

def vacuum():
    engine.execute('VACUUM')

def open_session():
    return Session()
